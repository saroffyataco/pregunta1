let length = 30;
let k = 2;
let numbers = [];

for (; k < length; k++) {

  if (primo(k)) {
    numbers.push(k);
  }
  
}

console.log(numbers);

function primo(number) {

  for (let i = 2; i < number; i++) {

    if (number % i === 0) {
      return false;
    }

  }
  return number !== 1;
}